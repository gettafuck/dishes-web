import { User } from '../../../app/models/user/User';
import { userAvatars, userCovers } from '../../images/data/images';

export const users = [
  /** @info: unauthorized user */
  new User({
    membership: 'basic',
    roles: [],
    images: [],
    id: '',
    name: '',
    lastName: ''
  }),

  /** @info: authorized user with basic rights */
  new User({
    membership: 'basic',
    roles: ['user'],
    images: [
      userAvatars[0],
      userCovers[0]
    ],
    id: '1',
    name: 'Alex',
    lastName: 'Smith'
  }),

  /** @info: moderator */
  new User({
    membership: 'basic',
    roles: ['moderator'],
    images: [
      userAvatars[1],
      userCovers[1]
    ],
    id: '2',
    name: 'Edvin',
    lastName: 'van der Saar'
  }),

  /** @info: admin */
  new User({
    membership: 'basic',
    roles: ['admin'],
    images: [
      userAvatars[2],
      userCovers[2]
    ],
    id: '3',
    name: 'John',
    lastName: 'Arne Riese'
  }),

  /** @info: superadmin */
  new User({
    membership: 'basic',
    roles: ['superadmin'],
    images: [
      userAvatars[3],
      userCovers[3]
    ],
    id: '4',
    name: 'Nemania',
    lastName: 'Vidic'
  })
];
