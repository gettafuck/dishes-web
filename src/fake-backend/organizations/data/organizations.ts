import { Organization } from '../../../app/models/organization/Organization';
import { organizationTypes } from '../../set-types/data/set-types';
import { organizationLogos } from '../../images/data/images';
import { users } from '../../users/data/users';

export const organizations = [
  new Organization({
    types: [
      organizationTypes[0], // restaurant
      organizationTypes[1]  // hotel
    ],
    images: [
      organizationLogos[0]
    ],
    creatorId: users[4].id,
    creator: users[4],
    ownerId: users[4].id,
    owner: users[4],
    id: '1',
    name: 'Restaurant Casemiro',
    description: 'Super awesome restaurant!',
    dishes: []
  }),
  new Organization({
    types: [
      organizationTypes[1], // hotel
      organizationTypes[8]  // wine_pub
    ],
    images: [
      organizationLogos[1]
    ],
    creatorId: users[4].id,
    creator: users[4],
    ownerId: users[4].id,
    owner: users[4],
    id: '2',
    name: 'Hotel Star',
    description: 'Super awesome hotel!',
    dishes: []
  }),
  new Organization({
    types: [
      organizationTypes[2], // night_club
    ],
    images: [
      organizationLogos[2]
    ],
    creatorId: users[4].id,
    creator: users[4],
    ownerId: users[4].id,
    owner: users[4],
    id: '3',
    name: 'Ibiza Night Club',
    description: 'Super awesome night club!',
    dishes: []
  }),
  new Organization({
    types: [
      organizationTypes[3], // coworking
    ],
    images: [
      organizationLogos[3]
    ],
    creatorId: users[4].id,
    creator: users[4],
    ownerId: users[4].id,
    owner: users[4],
    id: '4',
    name: 'SuperHub Coworking',
    description: 'Super awesome coworking!',
    dishes: []
  }),
  new Organization({
    types: [
      organizationTypes[4], // cafe
    ],
    images: [
      organizationLogos[4]
    ],
    creatorId: users[4].id,
    creator: users[4],
    ownerId: users[4].id,
    owner: users[4],
    id: '5',
    name: 'Lemon Cafe',
    description: 'Super awesome cafe!',
    dishes: []
  }),
  new Organization({
    types: [
      organizationTypes[5], // bakery
    ],
    images: [
      organizationLogos[5]
    ],
    creatorId: users[4].id,
    creator: users[4],
    ownerId: users[4].id,
    owner: users[4],
    id: '6',
    name: 'Braunini Bakery',
    description: 'Super awesome bakery!',
    dishes: []
  }),
  new Organization({
    types: [
      organizationTypes[6], // coffee_shop
    ],
    images: [
      organizationLogos[6]
    ],
    creatorId: users[4].id,
    creator: users[4],
    ownerId: users[4].id,
    owner: users[4],
    id: '7',
    name: 'Aroma Coffee Shop',
    description: 'Super awesome coffee shop!',
    dishes: []
  }),
  new Organization({
    types: [
      organizationTypes[7], // tea_shop
    ],
    images: [
      organizationLogos[7]
    ],
    creatorId: users[4].id,
    creator: users[4],
    ownerId: users[4].id,
    owner: users[4],
    id: '8',
    name: 'China Tea Shop',
    description: 'Super awesome tea shop!',
    dishes: []
  }),
  new Organization({
    types: [
      organizationTypes[8], // wine_pub
    ],
    images: [
      organizationLogos[8]
    ],
    creatorId: users[4].id,
    creator: users[4],
    ownerId: users[4].id,
    owner: users[4],
    id: '9',
    name: 'Bordo Wine Pub',
    description: 'Super awesome wine pub!',
    dishes: []
  }),
  new Organization({
    types: [
      organizationTypes[9], // beer_pub
    ],
    images: [
      organizationLogos[9]
    ],
    creatorId: users[4].id,
    creator: users[4],
    ownerId: users[4].id,
    owner: users[4],
    id: '10',
    name: 'Corvin Beer Pub',
    description: 'Super awesome beer pub!',
    dishes: []
  })
];
