import { PaginationItem } from '../../../app/core/models/pagination-item/PaginationItem';
import { Ingredient } from '../../../app/models/ingredient/Ingredient';
import { ingredients } from './ingredients';
import { PaginatedGroup } from '../../../app/core/models/paginated-group/PaginatedGroup';
import { ingredientTypes } from '../../set-types/data/set-types';

const paginated__vegan: PaginatedGroup<Ingredient> [] = [
	new PaginatedGroup({
		group: ingredientTypes[0], // vegan
		page: new PaginationItem({
			allNumber: 3,
			number: 1,
			elements: [
				ingredients[5], // chickpeas
				ingredients[9], // asparagus
			]
		})
	}),

	new PaginatedGroup({
		group: ingredientTypes[0], // vegan
		page: new PaginationItem({
			allNumber: 3,
			number: 2,
			elements: [
				ingredients[10], // dill
				ingredients[8], // cucumber
			]
		})
	}),

	new PaginatedGroup({
		group: ingredientTypes[0], // vegan
		page: new PaginationItem({
			allNumber: 3,
			number: 3,
			elements: [
				ingredients[7], // tomato
			]
		})
	})

]; // paginated__vegan - end


const paginated__milk_products: PaginatedGroup<Ingredient> [] = [
	new PaginatedGroup({
		group: ingredientTypes[1], // milk_products
		page: new PaginationItem({
			allNumber: 2,
			number: 1,
			elements: [
				ingredients[0], // cheese
				ingredients[2], // milk
			]
		})
	}),

	new PaginatedGroup({
		group: ingredientTypes[1], // milk_products
		page: new PaginationItem({
			allNumber: 2,
			number: 2,
			elements: [
				ingredients[11], // sour cream
			]
		})
	})

]; // paginated__milk_products - end


const paginated__berries: PaginatedGroup<Ingredient> [] = [
	new PaginatedGroup({
		group: ingredientTypes[4], // berries
		page: new PaginationItem({
			allNumber: 1,
			number: 1,
			elements: [
				ingredients[1], // strawberry
			]
		})
	})

]; // paginated__berries - end


const paginated__meat: PaginatedGroup<Ingredient> [] = [
	new PaginatedGroup({
		group: ingredientTypes[5], // meat
		page: new PaginationItem({
			allNumber: 1,
			number: 1,
			elements: [
				ingredients[3], // ham
			]
		})
	})

]; // paginated__meat - end


const paginated__eggs: PaginatedGroup<Ingredient> [] = [
	new PaginatedGroup({
		group: ingredientTypes[6], // eggs
		page: new PaginationItem({
			allNumber: 1,
			number: 1,
			elements: [
				ingredients[4], // chicken eggs
			]
		})
	})

]; // paginated__eggs - end


const paginated__beans: PaginatedGroup<Ingredient> [] = [
	new PaginatedGroup({
		group: ingredientTypes[7], // beans
		page: new PaginationItem({
			allNumber: 1,
			number: 1,
			elements: [
				ingredients[5], // chickpeas
			]
		})
	})

]; // paginated__beans - end


const paginated__bread: PaginatedGroup<Ingredient> [] = [
	new PaginatedGroup({
		group: ingredientTypes[7], // bread
		page: new PaginationItem({
			allNumber: 1,
			number: 1,
			elements: [
				ingredients[6], // bread
			]
		})
	})

]; // paginated__bread - end


const paginated__cereals: PaginatedGroup<Ingredient> [] = [
	new PaginatedGroup({
		group: ingredientTypes[2], // cereals
		page: new PaginationItem({
			allNumber: 1,
			number: 1,
			elements: [
				ingredients[6], // bread
			]
		})
	})

]; // paginated__cereals - end


const paginated__vegetables: PaginatedGroup<Ingredient> [] = [
	new PaginatedGroup({
		group: ingredientTypes[9], // vegetables
		page: new PaginationItem({
			allNumber: 2,
			number: 1,
			elements: [
				ingredients[7], // tomato
				ingredients[8], // cucumber
			]
		})
	}),

	new PaginatedGroup({
		group: ingredientTypes[9], // vegetables
		page: new PaginationItem({
			allNumber: 2,
			number: 2,
			elements: [
				ingredients[9], // asparagus
				ingredients[10], // dill
			]
		})
	}),

]; // paginated__vegetables - end


/**
 * @info: at first we need to load all groups with initial page of elements, i.e with page number 1,
 * Then we can request other pages for a group. 
 */
export const initial_pages__allGroups: PaginatedGroup<Ingredient> [] = [
	paginated__vegan[0],
	paginated__milk_products[0],
	paginated__berries[0],
	paginated__meat[0],
	paginated__eggs[0],
	paginated__beans[0],
	paginated__bread[0],
	paginated__cereals[0],
	paginated__vegetables[0]
];

/**
 * @info: needed to get group by name
 */
export const named__paginated__groups: any = {
	vegan: paginated__vegan,
	milk_products: paginated__milk_products,
	berries: paginated__berries,
	meat: paginated__meat,
	eggs: paginated__eggs,
	beans: paginated__beans,
	bread: paginated__bread,
	cereals: paginated__cereals,
	vegetables: paginated__vegetables
}

