import { Ingredient } from '../../../app/models/ingredient/Ingredient';
import { ingredientTypes } from '../../set-types/data/set-types';
import { ingredientIcons, ingredientCovers } from '../../images/data/images';
import { users } from '../../users/data/users';

export const ingredients = [
  new Ingredient({
    name: 'cheese',
    types: [
      ingredientTypes[1] // milk_products
    ],
    images: [
      ingredientIcons[0], // cheese
      ingredientCovers[0] // cheese
    ],
    creatorId: users[4].id,
    creator: users[4],
    approverId: users[4].id,
    approver: users[4],
    id: '1',
    weight: 0
  }),
  new Ingredient({
    name: 'strawberry',
    types: [
      ingredientTypes[4] // berries
    ],
    images: [
      ingredientIcons[1], // strawberry
      ingredientCovers[1] // strawberry
    ],
    creatorId: users[4].id,
    creator: users[4],
    approverId: users[4].id,
    approver: users[4],
    id: '2',
    weight: 0
  }),
  new Ingredient({
    name: 'milk',
    types: [
      ingredientTypes[1] // milk_products
    ],
    images: [
      ingredientIcons[2], // milk
      ingredientCovers[2] // milk
    ],
    creatorId: users[4].id,
    creator: users[4],
    approverId: users[4].id,
    approver: users[4],
    id: '3',
    weight: 0
  }),
  new Ingredient({
    name: 'ham',
    types: [
      ingredientTypes[5] // meat
    ],
    images: [
      ingredientIcons[3], // ham
      ingredientCovers[3] // ham
    ],
    creatorId: users[4].id,
    creator: users[4],
    approverId: users[4].id,
    approver: users[4],
    id: '4',
    weight: 0
  }),
  new Ingredient({
    name: 'chicken eggs',
    types: [
      ingredientTypes[6] // eggs
    ],
    images: [
      ingredientIcons[4], // chicken eggs
      ingredientCovers[4] // chicken eggs
    ],
    creatorId: users[4].id,
    creator: users[4],
    approverId: users[4].id,
    approver: users[4],
    id: '5',
    weight: 0
  }),
  new Ingredient({
    name: 'chickpeas',
    types: [
      ingredientTypes[7], // beans
      ingredientTypes[0], // vegan
    ],
    images: [
      ingredientIcons[5], // chickpeas
      ingredientCovers[5] // chickpeas
    ],
    creatorId: users[4].id, // superadmin
    creator: users[4], // superadmin
    approverId: users[4].id, // superadmin
    approver: users[4], // superadmin
    id: '6',
    weight: 0
  }),
  new Ingredient({
    name: 'bread',
    types: [
      ingredientTypes[8], // bread
      ingredientTypes[2]  // cereals
    ],
    images: [
      ingredientIcons[6], // bread
      ingredientCovers[6] // bread
    ],
    creatorId: users[4].id, // superadmin
    creator: users[4], // superadmin
    approverId: users[4].id, // superadmin
    approver: users[4], // superadmin
    id: '7',
    weight: 0
  }),
  new Ingredient({
    name: 'tomato',
    types: [
      ingredientTypes[9], // vegetables
      ingredientTypes[0], // vegan
    ],
    images: [
      ingredientIcons[7], // tomato
      ingredientCovers[7] // tomato
    ],
    creatorId: users[4].id, // superadmin
    creator: users[4], // superadmin
    approverId: users[4].id, // superadmin
    approver: users[4], // superadmin
    id: '8',
    weight: 0
  }),
  new Ingredient({
    name: 'cucumber',
    types: [
      ingredientTypes[9], // vegetables
      ingredientTypes[0], // vegan
    ],
    images: [
      ingredientIcons[8], // cucumber
      ingredientCovers[8] // cucumber
    ],
    creatorId: users[4].id, // superadmin
    creator: users[4], // superadmin
    approverId: users[4].id, // superadmin
    approver: users[4], // superadmin
    id: '9',
    weight: 0
  }),
  new Ingredient({
    name: 'asparagus', // спаржа
    types: [
      ingredientTypes[9], // vegetables
      ingredientTypes[0], // vegan
    ],
    images: [
      ingredientIcons[9], // asparagus
      ingredientCovers[9] // asparagus
    ],
    creatorId: users[4].id, // superadmin
    creator: users[4], // superadmin
    approverId: users[4].id, // superadmin
    approver: users[4], // superadmin
    id: '10',
    weight: 0
  }),
  new Ingredient({
    name: 'dill', // укроп
    types: [
      ingredientTypes[9], // vegetables
      ingredientTypes[0], // vegan
    ],
    images: [
      ingredientIcons[10], // dill
      ingredientCovers[10] // dill
    ],
    creatorId: users[4].id, // superadmin
    creator: users[4], // superadmin
    approverId: users[4].id, // superadmin
    approver: users[4], // superadmin
    id: '11',
    weight: 0
  }),
  new Ingredient({
    name: 'sour cream',
    types: [
      ingredientTypes[1] // milk_products
    ],
    images: [
      ingredientIcons[11], // sour cream
      ingredientCovers[11] // sour cream
    ],
    creatorId: users[4].id, // superadmin
    creator: users[4], // superadmin
    approverId: users[4].id, // superadmin
    approver: users[4], // superadmin
    id: '12',
    weight: 0
  })
];
