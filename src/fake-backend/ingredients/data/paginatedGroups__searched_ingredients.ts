import { PaginationItem } from '../../../app/core/models/pagination-item/PaginationItem';
import { Ingredient } from '../../../app/models/ingredient/Ingredient';
import { ingredients } from './ingredients';
import { PaginatedGroup } from '../../../app/core/models/paginated-group/PaginatedGroup';
import { ingredientTypes } from '../../set-types/data/set-types';

/** 
 * @info: paginated groups what contain searched ingredients 
 * 	that contain letter `a` in the value of `name` property
 */

const paginated__vegan__searched: PaginatedGroup<Ingredient> [] = [
	new PaginatedGroup({
		group: ingredientTypes[0], // vegan
		page: new PaginationItem({
			allNumber: 2,
			number: 1,
			elements: [
				ingredients[9], // asparagus
				ingredients[5], // chickpeas
			]
		})
	}),

	new PaginatedGroup({
		group: ingredientTypes[0], // vegan
		page: new PaginationItem({
			allNumber: 2,
			number: 2,
			elements: [
				ingredients[7], // tomato
			]
		})
	})

]; // paginated__vegan - end


const paginated__milk_products__searched: PaginatedGroup<Ingredient> [] = [
	new PaginatedGroup({
		group: ingredientTypes[1], // milk_products
		page: new PaginationItem({
			allNumber: 1,
			number: 1,
			elements: [
				ingredients[11], // sour cream
			]
		})
	})

]; // paginated__milk_products - end


const paginated__berries__searched: PaginatedGroup<Ingredient> [] = [
	new PaginatedGroup({
		group: ingredientTypes[4], // berries
		page: new PaginationItem({
			allNumber: 1,
			number: 1,
			elements: [
				ingredients[1], // strawberry
			]
		})
	})

]; // paginated__berries - end


const paginated__meat__searched: PaginatedGroup<Ingredient> [] = [
	new PaginatedGroup({
		group: ingredientTypes[5], // meat
		page: new PaginationItem({
			allNumber: 1,
			number: 1,
			elements: [
				ingredients[3], // ham
			]
		})
	})

]; // paginated__meat - end


const paginated__beans__searched: PaginatedGroup<Ingredient> [] = [
	new PaginatedGroup({
		group: ingredientTypes[7], // beans
		page: new PaginationItem({
			allNumber: 1,
			number: 1,
			elements: [
				ingredients[5], // chickpeas
			]
		})
	})

]; // paginated__beans - end


const paginated__bread__searched: PaginatedGroup<Ingredient> [] = [
	new PaginatedGroup({
		group: ingredientTypes[7], // bread
		page: new PaginationItem({
			allNumber: 1,
			number: 1,
			elements: [
				ingredients[6], // bread
			]
		})
	})

]; // paginated__bread - end


const paginated__cereals__searched: PaginatedGroup<Ingredient> [] = [
	new PaginatedGroup({
		group: ingredientTypes[2], // cereals
		page: new PaginationItem({
			allNumber: 1,
			number: 1,
			elements: [
				ingredients[6], // bread
			]
		})
	})

]; // paginated__cereals - end


const paginated__vegetables__searched: PaginatedGroup<Ingredient> [] = [
	new PaginatedGroup({
		group: ingredientTypes[9], // vegetables
		page: new PaginationItem({
			allNumber: 1,
			number: 1,
			elements: [
				ingredients[7], // tomato
				ingredients[9], // asparagus
			]
		})
	})

]; // paginated__vegetables - end


/**
 * @info: 
 * At first we need to load all groups that contain searched elements.
 * At first each group contains page number 1.
 * Then we can request other pages for each group. 
 */
export const pages__groups__searched_ingredients: PaginatedGroup<Ingredient> [] = [
	paginated__vegan__searched[0],
	paginated__milk_products__searched[0],
	paginated__berries__searched[0],
	paginated__meat__searched[0],
	paginated__beans__searched[0],
	paginated__bread__searched[0],
	paginated__cereals__searched[0],
	paginated__vegetables__searched[0]
];


/**
 * @info: needed to get group by name
 */
export const named__paginated__groups__searched: any = {
	vegan: paginated__vegan__searched,
	milk_products: paginated__milk_products__searched,
	berries: paginated__berries__searched,
	meat: paginated__meat__searched,
	beans: paginated__beans__searched,
	bread: paginated__bread__searched,
	cereals: paginated__cereals__searched,
	vegetables: paginated__vegetables__searched
}
