import {
	HttpRequest, 
	HttpEvent,
	HttpHandler,
	HttpInterceptor,
	HttpResponse
} from '@angular/common/http';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs';

import { StringService } from '../../app/core/services/string/string.service';

import { BackendResponse } from '../../app/core/models/backend-response/BackendResponse';

import { Ingredient } from '../../app/models/ingredient/Ingredient';

import { Url } from '../../app/core/models/url/Url';
import { PaginatedGroup } from '../../app/core/models/paginated-group/PaginatedGroup';
import { initial_pages__allGroups, named__paginated__groups } from './data/paginatedGroups';
import { pages__groups__searched_ingredients, named__paginated__groups__searched } from './data/paginatedGroups__searched_ingredients';

@Injectable()
export class IngredientsHttpInterceptor implements HttpInterceptor {

	constructor(
		private stringService: StringService
	) { }

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		// console.log(`\n${req.method} ${req.url} was intercepted by mock backend (IngredientsHttpInterceptor)`);


		const url: Url = this.stringService.parseUrl(req.url);
		/**
		 * @test
		 */
		// console.log('\nparsed url - ', url );



		/** 
		 * GET ingredients ? name = a & group = vegan & page = {n}
		 */
		if (
			req.url.indexOf('/ingredients?') >= 0  &&  
			url.get('name').values[0]  &&  
			url.get('group').values[0] === 'vegan'  &&  
			url.get('page').values[0]  &&  
			req.method === 'GET'
		) {

			const page: number = <number> +url.get('page').values[0];
			const group: string = url.get('group').values[0];

			const response: BackendResponse< PaginatedGroup<Ingredient> > = new BackendResponse({
				data: named__paginated__groups__searched[ group ][ page - 1 ], /**
				 * @info: page enumeration starts from 1, not from 0 as in arrays
				 */
				statusText: 'OK'
			});

			return of( new HttpResponse({body: response}) );
		}

		/** 
		 * GET ingredients ? name = a
		 * 
		 * @info: search ingredients that contain letter `a` in the value of `name` property
		 */
		if (
			req.url.indexOf('/ingredients?') >= 0  &&  
			url.get('name').values[0]  &&  
			req.method === 'GET' 
		) {
			const name: string = url.get('name').values[0];

			const response: BackendResponse< PaginatedGroup<Ingredient> [] > = new BackendResponse({
				data: pages__groups__searched_ingredients, 
				statusText: 'OK'
			});

			return of( new HttpResponse({body: response}) );
		}



		/** 
		 * GET ingredients ? group = vegan & page = {n}
		 */
		if (
			req.url.indexOf('/ingredients?') >= 0  &&  
			url.get('group').values[0]  &&  
			url.get('page').values[0]  &&  
			req.method === 'GET'
		) {

			const page: number = <number> +url.get('page').values[0];
			const group: string = url.get('group').values[0];

			const response: BackendResponse< PaginatedGroup<Ingredient> > = new BackendResponse({
				data: named__paginated__groups[ group ][ page - 1 ], /**
				 * @info: page enumeration starts from 1, not from 0 as in arrays
				 */
				statusText: 'OK'
			});

			return of( new HttpResponse({body: response}) );
		}



		/** 
		 * GET ingredients
		 */
		if (req.url.indexOf('/ingredients') >= 0  &&  req.method === 'GET' ) {
			const response: BackendResponse< PaginatedGroup<Ingredient> [] > = new BackendResponse({
				data: initial_pages__allGroups,
				statusText: 'OK',
			});

			return of( new HttpResponse({body: response}) );
		}


		// pass the request forward for processing
		return next.handle(req);

	}
}

