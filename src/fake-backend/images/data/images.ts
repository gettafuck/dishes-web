import { Image } from '../../../app/models/image/Image';

// role: logo
export const userAvatars: Image[] = [
  /** @info: user logo (i.e. avatar) */
  new Image({
    role: 'logo',
    set: 'user',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/userAvatars/female-41.jpg',
    description: 'my avatar',
    id: '1'
  }),
  /** @info: user logo (i.e. avatar) */
  new Image({
    role: 'logo',
    set: 'user',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/userAvatars/female-46.jpg',
    description: 'my avatar',
    id: '2'
  }),
  /** @info: user logo (i.e. avatar) */
  new Image({
    role: 'logo',
    set: 'user',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/userAvatars/female-60.jpg',
    description: 'my avatar',
    id: '3'
  }),
  /** @info: user logo (i.e. avatar) */
  new Image({
    role: 'logo',
    set: 'user',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/userAvatars/male-22.jpg',
    description: 'my avatar',
    id: '4'
  })
];
//userAvatars

// role: cover
export const userCovers: Image[] = [
  /** @info: user cover */
  new Image({
    role: 'cover',
    set: 'user',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/userCovers/user-cover-1.jpg',
    description: 'my cover',
    id: '101'
  }),
  /** @info: user cover */
  new Image({
    role: 'cover',
    set: 'user',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/userCovers/user-cover-2.jpg',
    description: 'my cover',
    id: '102'
  }),
  /** @info: user cover */
  new Image({
    role: 'cover',
    set: 'user',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/userCovers/user-cover-3.jpg',
    description: 'my cover',
    id: '103'
  }),
  /** @info: user cover */
  new Image({
    role: 'cover',
    set: 'user',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/userCovers/user-cover-4.jpg',
    description: 'my cover',
    id: '104'
  })
];
// userCovers


// role: logo
export const organizationLogos: Image[] = [
  new Image({
    role: 'logo',
    set: 'organization',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/organizationLogos/1-restaurant.jpg',
    description: 'restaurant logo',
    id: '201'
  }),
  new Image({
    role: 'logo',
    set: 'organization',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/organizationLogos/2-hotel.jpg',
    description: 'hotel logo',
    id: '202'
  }),
  new Image({
    role: 'logo',
    set: 'organization',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/organizationLogos/3-night_club.jpg',
    description: 'night club logo',
    id: '203'
  }),
  new Image({
    role: 'logo',
    set: 'organization',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/organizationLogos/4-coworking.png',
    description: 'coworking logo',
    id: '204'
  }),
  new Image({
    role: 'logo',
    set: 'organization',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/organizationLogos/5-cafe.jpg',
    description: 'cafe logo',
    id: '205'
  }),
  new Image({
    role: 'logo',
    set: 'organization',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/organizationLogos/6-bakery.png',
    description: 'bakery logo',
    id: '206'
  }),
  new Image({
    role: 'logo',
    set: 'organization',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/organizationLogos/7-coffee_shop.jpg',
    description: 'coffee shop logo',
    id: '207'
  }),
  new Image({
    role: 'logo',
    set: 'organization',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/organizationLogos/8-tea_shop.png',
    description: 'tea shop logo',
    id: '208'
  }),
  new Image({
    role: 'logo',
    set: 'organization',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/organizationLogos/9-wine_pub.jpg',
    description: 'wine pub logo',
    id: '209'
  }),
  new Image({
    role: 'logo',
    set: 'organization',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/organizationLogos/10-beer_pub.jpg',
    description: 'beer pub logo',
    id: '210'
  })
];
// organizationLogos

// role: cover
export const organizationCovers: Image[] = [
  new Image({
    role: 'cover',
    set: 'organization',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/organizationCovers/1-restaurant.jpg',
    description: 'restaurant cover',
    id: '301'
  }),
  new Image({
    role: 'cover',
    set: 'organization',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/organizationCovers/2-hotel.jpg',
    description: 'hotel cover',
    id: '302'
  }),
  new Image({
    role: 'cover',
    set: 'organization',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/organizationCovers/3-night_club.jpg',
    description: 'night club cover',
    id: '303'
  }),
  new Image({
    role: 'cover',
    set: 'organization',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/organizationCovers/4-coworking.png',
    description: 'coworking cover',
    id: '304'
  }),
  new Image({
    role: 'cover',
    set: 'organization',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/organizationCovers/5-cafe.jpg',
    description: 'cafe cover',
    id: '305'
  }),
  new Image({
    role: 'cover',
    set: 'organization',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/organizationCovers/6-bakery.png',
    description: 'bakery cover',
    id: '306'
  }),
  new Image({
    role: 'cover',
    set: 'organization',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/organizationCovers/7-coffee_shop.jpg',
    description: 'coffee shop cover',
    id: '307'
  }),
  new Image({
    role: 'cover',
    set: 'organization',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/organizationCovers/8-tea_shop.png',
    description: 'tea shop cover',
    id: '308'
  }),
  new Image({
    role: 'cover',
    set: 'organization',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/organizationCovers/9-wine_pub.jpg',
    description: 'wine pub cover',
    id: '309'
  }),
  new Image({
    role: 'cover',
    set: 'organization',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/organizationCovers/10-beer_pub.jpg',
    description: 'beer pub cover',
    id: '310'
  })
];
//organizationCovers


// role: logo
export const ingredientIcons: Image[] = [
  new Image({
    role: 'logo',
    set: 'ingredient',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/ingredientIcons/1-cheese.svg',
    description: 'cheese linear icon',
    id: '401'
  }),
  new Image({
    role: 'logo',
    set: 'ingredient',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/ingredientIcons/2-strawberry.svg',
    description: 'strawberry linear icon',
    id: '402'
  }),
  new Image({
    role: 'logo',
    set: 'ingredient',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/ingredientIcons/3-milk.svg',
    description: 'milk linear icon',
    id: '403'
  }),
  new Image({
    role: 'logo',
    set: 'ingredient',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/ingredientIcons/4-ham.svg',
    description: 'ham linear icon',
    id: '404'
  }),
  new Image({
    role: 'logo',
    set: 'ingredient',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/ingredientIcons/5-chicken-eggs.svg',
    description: 'chicken eggs linear icon',
    id: '405'
  }),
  new Image({
    role: 'logo',
    set: 'ingredient',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/ingredientIcons/6-chickpeas.svg',
    description: 'chickpeas linear icon',
    id: '406'
  }),
  new Image({
    role: 'logo',
    set: 'ingredient',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/ingredientIcons/7-bread.svg',
    description: 'bread linear icon',
    id: '407'
  }),
  new Image({
    role: 'logo',
    set: 'ingredient',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/ingredientIcons/8-tomato.svg',
    description: 'tomato linear icon',
    id: '408'
  }),
  new Image({
    role: 'logo',
    set: 'ingredient',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/ingredientIcons/9-cucumber.svg',
    description: 'cucumber linear icon',
    id: '409'
  }),
  new Image({
    role: 'logo',
    set: 'ingredient',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/ingredientIcons/10-asparagus.svg',
    description: 'asparagus linear icon',
    id: '410'
  }),
  new Image({
    role: 'logo',
    set: 'ingredient',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/ingredientIcons/11-dill.svg',
    description: 'dill linear icon',
    id: '411'
  }),
  new Image({
    role: 'logo',
    set: 'ingredient',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/ingredientIcons/12-sour-cream.svg',
    description: 'sour cream linear icon',
    id: '412'
  })
];
// ingredientIcons

// role: cover
export const ingredientCovers: Image[] = [
  new Image({
    role: 'cover',
    set: 'ingredient',
    creatorId: '', // superadmin
    creator: null, // superadmin
    url: 'assets/test/images/ingredientCovers/1-cheese.jpg',
    description: 'cheese cover',
    id: '501'
  }),
  new Image({
    role: 'cover',
    set: 'ingredient',
    creatorId: '', // superadmin
    creator: null, // superadmin
    url: 'assets/test/images/ingredientCovers/2-strawberry.jpg',
    description: 'strawberry cover',
    id: '502'
  }),
  new Image({
    role: 'cover',
    set: 'ingredient',
    creatorId: '', // superadmin
    creator: null, // superadmin
    url: 'assets/test/images/ingredientCovers/3-milk.jpg',
    description: 'milk cover',
    id: '503'
  }),
  new Image({
    role: 'cover',
    set: 'ingredient',
    creatorId: '', // superadmin
    creator: null, // superadmin
    url: 'assets/test/images/ingredientCovers/4-ham.jpg',
    description: 'ham cover',
    id: '504'
  }),
  new Image({
    role: 'cover',
    set: 'ingredient',
    creatorId: '', // superadmin
    creator: null, // superadmin
    url: 'assets/test/images/ingredientCovers/5-chicken-eggs.jpg',
    description: 'chicken eggs cover',
    id: '505'
  }),
  new Image({
    role: 'cover',
    set: 'ingredient',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/ingredientCovers/6-chickpeas.jpg',
    description: 'chickpeas cover',
    id: '506'
  }),
  new Image({
    role: 'cover',
    set: 'ingredient',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/ingredientCovers/7-bread.jpg',
    description: 'bread cover',
    id: '507'
  }),
  new Image({
    role: 'cover',
    set: 'ingredient',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/ingredientCovers/8-tomato.jpg',
    description: 'tomato cover',
    id: '508'
  }),
  new Image({
    role: 'cover',
    set: 'ingredient',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/ingredientCovers/9-cucumber.jpg',
    description: 'cucumber cover',
    id: '509'
  }),
  new Image({
    role: 'cover',
    set: 'ingredient',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/ingredientCovers/10-asparagus.jpg',
    description: 'asparagus cover',
    id: '510'
  }),
  new Image({
    role: 'cover',
    set: 'ingredient',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/ingredientCovers/11-dill.jpg',
    description: 'dill cover',
    id: '511'
  }),
  new Image({
    role: 'cover',
    set: 'ingredient',
    creatorId: '',
    creator: null,
    url: 'assets/test/images/ingredientCovers/12-sour-cream.jpg',
    description: 'sour cream cover',
    id: '512'
  })
];
// ingredientCovers


// role: ''
export const dishImages: Image[] = [
  new Image({
    role: '',
    set: 'dish',
    creatorId: '', // superadmin
    creator: null, // superadmin
    url: 'assets/test/images/dishImages/1-Vegan-Sandwitch.jpg',
    description: 'Vegan Sandwitch image',
    id: '601'
  }),
  new Image({
    role: '',
    set: 'dish',
    creatorId: '', // superadmin
    creator: null, // superadmin
    url: '../../../assets/test/images/dishImages/2-Ham-under-Strawberry-Souse.jpg',
    description: 'Ham under Strawberry Souse image',
    id: '602'
  }),
  new Image({
    role: '',
    set: 'dish',
    creatorId: '', // superadmin
    creator: null, // superadmin
    url: '../../../assets/test/images/dishImages/3-Starwberry-Sufle.jpg',
    description: 'Ham under Strawberry Souse image',
    id: '603'
  }),
  new Image({
    role: '',
    set: 'dish',
    creatorId: '', // superadmin
    creator: null, // superadmin
    url: '../../../assets/test/images/dishImages/4-Ham-Beef.jpg',
    description: 'Ham under Strawberry Souse image',
    id: '604'
  }),
  new Image({
    role: '',
    set: 'dish',
    creatorId: '', // superadmin
    creator: null, // superadmin
    url: '../../../assets/test/images/dishImages/5-Creamy-Soup.jpg',
    description: 'Ham under Strawberry Souse image',
    id: '605'
  }),
  new Image({
    role: '',
    set: 'dish',
    creatorId: '', // superadmin
    creator: null, // superadmin
    url: '../../../assets/test/images/dishImages/6-Summer-Salad.jpg',
    description: 'Ham under Strawberry Souse image',
    id: '606'
  })
];
// dishImages

// role: cover
export const dishCovers: Image[] = [
  new Image({
    role: 'cover',
    set: 'dish',
    creatorId: '', // superadmin
    creator: null, // superadmin
    url: 'assets/test/images/dishCovers/1-Vegan-Sandwitch.jpg',
    description: 'Vegan Sandwitch cover',
    id: '701'
  }),
  new Image({
    role: '',
    set: 'dish',
    creatorId: '', // superadmin
    creator: null, // superadmin
    url: '../../../assets/test/images/dishCovers/2-Ham-under-Strawberry-Souse.jpg',
    description: 'Ham under Strawberry Souse image',
    id: '702'
  }),
  new Image({
    role: '',
    set: 'dish',
    creatorId: '', // superadmin
    creator: null, // superadmin
    url: '../../../assets/test/images/dishCovers/3-Starwberry-Sufle.jpg',
    description: 'Ham under Strawberry Souse image',
    id: '703'
  }),
  new Image({
    role: '',
    set: 'dish',
    creatorId: '', // superadmin
    creator: null, // superadmin
    url: '../../../assets/test/images/dishCovers/4-Ham-Beef.jpg',
    description: 'Ham under Strawberry Souse image',
    id: '704'
  }),
  new Image({
    role: '',
    set: 'dish',
    creatorId: '', // superadmin
    creator: null, // superadmin
    url: '../../../assets/test/images/dishCovers/5-Creamy-Soup.jpg',
    description: 'Ham under Strawberry Souse image',
    id: '705'
  }),
  new Image({
    role: '',
    set: 'dish',
    creatorId: '', // superadmin
    creator: null, // superadmin
    url: '../../../assets/test/images/dishCovers/6-Summer-Salad.jpg',
    description: 'Ham under Strawberry Souse image',
    id: '706'
  })
];

// dishCovers
