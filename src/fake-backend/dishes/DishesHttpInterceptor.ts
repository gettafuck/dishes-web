import {
	HttpRequest, 
	HttpEvent,
	HttpHandler,
	HttpInterceptor,
	HttpResponse
} from '@angular/common/http';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs';

import { StringService } from '../../app/core/services/string/string.service';

import { BackendResponse } from '../../app/core/models/backend-response/BackendResponse';

import { PaginationItem } from '../../app/core/models/pagination-item/PaginationItem';

import { Dish } from '../../app/models/dish/Dish';
import { paginatedDishes } from './data/paginatedDishes';
import { searchedPaginatedDishes } from './data/searchedPaginatedDishes';
import { Url } from '../../app/core/models/url/Url';

@Injectable()
export class DishesHttpInterceptor implements HttpInterceptor {

	constructor(
		private stringService: StringService
	) { }

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		// console.log(`\n${req.method} ${req.url} was intercepted by mock backend (DishesHttpInterceptor)`);

		/**
		 * @test
		 */
    // const fake_url = '/dishes?ingredients=rucola,dill&name=Fakie&userIds=1,2,3';
		// console.log( 'query params - ', this.stringService.parseUrl(fake_url) );


		const url: Url = this.stringService.parseUrl(req.url);


		/** 
		 * GET dishes ? search params...
		 */
		if (
			req.url.indexOf('/dishes?') >= 0  &&  
			url.get('ingredients').values[0]  &&  
			url.get('page').values[0]  &&  
			req.method === 'GET'
		) {
			console.log(`\n${req.method} ${req.url} was intercepted by mock backend (DishesHttpInterceptor)`);

			const page: number = <number> +url.get('page').values[0] || 1;

			const response: BackendResponse< PaginationItem<Dish> > = new BackendResponse({
				data: searchedPaginatedDishes[ page - 1 ], /**
				 * @info: page enumeration starts from 1, not from 0 as in arrays
				 */
	
				statusText: `Found dishes with ingredients - ${ url.get('ingredients').values.join(', ') }`,
			});

			/**
			 * @test
			 */
			// console.log( 'query params - ', url );

			return of( new HttpResponse({body: response}) );
		}


		/** 
		 * GET dishes
		 */
		if (
			req.url.indexOf('/dishes?') >= 0  &&  
			url.get('page').values[0]  &&  
			req.method === 'GET' 
		) {
			const page: number = <number> +url.get('page').values[0];

			const response: BackendResponse< PaginationItem<Dish> > = new BackendResponse({
				data: paginatedDishes[page - 1], /**
				 * @info: page enumeration starts from 1, not from 0 as in arrays
				 */
				statusText: 'OK',
			});

			return of( new HttpResponse({body: response}) );
		}

		// pass the request forward for processing
		return next.handle(req);
	}
}
