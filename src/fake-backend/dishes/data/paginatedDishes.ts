import { Dish } from '../../../app/models/dish/Dish';
import { dishes } from './dishes';
import { PaginationItem } from '../../../app/core/models/pagination-item/PaginationItem';

export const paginatedDishes: PaginationItem<Dish> [] = [
	new PaginationItem({
		elements: [
			dishes[0],
			dishes[1],
			dishes[2],
			dishes[3],
		],
		number: 1,
		allNumber: 2
	}),

	new PaginationItem({
		elements: [
			dishes[4],
			dishes[5],
		],
		number: 2,
		allNumber: 2
	})
];
