import { Dish } from '../../../app/models/dish/Dish';
import { organizations } from '../../organizations/data/organizations';
import { users } from '../../users/data/users';
import { ingredients } from '../../ingredients/data/ingredients';
import { dishTypes } from '../../set-types/data/set-types';
import { dishImages, dishCovers } from '../../images/data/images';

export const dishes: Dish[] = [
  new Dish({
    name: 'Vegan Sandwitch', // chickpeas(нут, турецкий горох), bread, tomato, cucumber  
    types: [
      dishTypes[0] // vegan
    ],
    images: [
      dishImages[0], // Vegan Sandwitch
      dishCovers[0]  // Vegan Sandwitch
    ],
    organizationId: organizations[0].id, // Restaurant Casemiro
    organization: organizations[0], // Restaurant Casemiro
    creatorId: users[4].id,
    creator: users[4], // superadmin
    id: '1',
    ingredients: [
      ingredients[5], // chickpeas
      ingredients[6], // bread
      ingredients[7], // tomato
      ingredients[8], // cucumber
    ]
  }),
  new Dish({
    name: 'Ham under Strawberry Souse',
    types: [
      dishTypes[1], // hot_dishes
      dishTypes[3] // meat_dishes
    ],
    images: [
      dishImages[1], // Ham under Strawberry Souse
      dishCovers[1]  // Ham under Strawberry Souse
    ],
    organizationId: organizations[1].id, // Hotel Star
    organization: organizations[1], // Hotel Star
    creatorId: users[4].id,
    creator: users[4], // superadmin
    id: '2',
    ingredients: [
      ingredients[3], // ham
      ingredients[1]  // strawberry
    ]
  }),
  new Dish({
    name: 'Strawberry Sufle',
    types: [
      dishTypes[2] // desserts
    ],
    images: [
      dishImages[2], // Strawberry Sufle
      dishCovers[2]  // Strawberry Sufle
    ],
    organizationId: organizations[0].id,
    organization: organizations[0],
    creatorId: users[4].id,
    creator: users[4], // superadmin
    id: '3',
    ingredients: [
      ingredients[1], // strawberry
      ingredients[2], // milk
      ingredients[4] // chicken eggs
    ]
  }),
  new Dish({
    name: 'Ham Beef',
    types: [
      dishTypes[3] // meat_dishes
    ],
    images: [
      dishImages[3], // Ham Beef
      dishCovers[3]  // Ham Beef
    ],
    organizationId: organizations[0].id,
    organization: organizations[0],
    creatorId: users[4].id,
    creator: users[4], // superadmin
    id: '4',
    ingredients: [
      ingredients[3] // ham
    ]
  }),
  new Dish({
    name: 'Creamy Soup', // sour cream(сметана), cheese, asparagus(спаржа), dill(укроп)
    types: [
      dishTypes[4] // soups
    ],
    images: [
      dishImages[4], // Creamy Soup
      dishCovers[4]  // Creamy Soup
    ],
    organizationId: organizations[4].id, // Lemon Cafe
    organization: organizations[4], // Lemon Cafe
    creatorId: users[4].id,
    creator: users[4], // superadmin
    id: '5',
    ingredients: [
      ingredients[12], // sour cream
      ingredients[0],  // cheese
      ingredients[9],  // asparagus
      ingredients[10], // dill
    ]
  }),
  new Dish({
    name: 'Summer Salad', // cucumber, tomato 
    types: [
      dishTypes[5] // salads
    ],
    images: [
      dishImages[5], // Summer Salad
      dishCovers[5]  // Summer Salad
    ],
    organizationId: organizations[4].id, // Lemon Cafe
    organization: organizations[4], // Lemon Cafe
    creatorId: users[4].id,
    creator: users[4], // superadmin
    id: '6',
    ingredients: [
      ingredients[8], // cucumber
      ingredients[7] // tomato
    ]
  })
];
