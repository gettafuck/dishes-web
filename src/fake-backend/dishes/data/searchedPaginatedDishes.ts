import { Dish } from '../../../app/models/dish/Dish';
import { dishes } from './dishes';
import { PaginationItem } from '../../../app/core/models/pagination-item/PaginationItem';

export const searchedPaginatedDishes: PaginationItem<Dish> [] = [
	new PaginationItem({
		elements: [
			dishes[3],
			dishes[0],
			dishes[4],
			dishes[2]
		],
		number: 1,
		allNumber: 2
	}),

	new PaginationItem({
		elements: [
			dishes[5],
			dishes[1]
		],
		number: 2,
		allNumber: 2
	})
];
