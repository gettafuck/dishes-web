import { SetType } from '../../../app/models/set-type/SetType';

export const organizationTypes = [
  new SetType({
    name: 'restaurant',
    role: 'organization',
    priority: 1,
    id: '1',
    color: '#fff',
    bgColor: '#d3003d'
  }),
  new SetType({
    name: 'hotel',
    role: 'organization',
    priority: 2,
    id: '2',
    color: '#fff',
    bgColor: '#d3003d'
  }),
  new SetType({
    name: 'night_club',
    role: 'organization',
    priority: 3,
    id: '3',
    color: '#fff',
    bgColor: '#d3003d'
  }),
  new SetType({
    name: 'coworking',
    role: 'organization',
    priority: 4,
    id: '4',
    color: '#fff',
    bgColor: '#d3003d'
  }),
  new SetType({
    name: 'cafe',
    role: 'organization',
    priority: 5,
    id: '5',
    color: '#fff',
    bgColor: '#d3003d'
  }),
  new SetType({
    name: 'bakery',
    role: 'organization',
    priority: 6,
    id: '6',
    color: '#fff',
    bgColor: '#d3003d'
  }),
  new SetType({
    name: 'coffee_shop',
    role: 'organization',
    priority: 7,
    id: '7',
    color: '#fff',
    bgColor: '#d3003d'
  }),
  new SetType({
    name: 'tea_shop',
    role: 'organization',
    priority: 8,
    id: '8',
    color: '#fff',
    bgColor: '#d3003d'
  }),
  new SetType({
    name: 'wine_pub',
    role: 'organization',
    priority: 9,
    id: '9',
    color: '#fff',
    bgColor: '#d3003d'
  }),
  new SetType({
    name: 'beer_pub',
    role: 'organization',
    priority: 10,
    id: '10',
    color: '#fff',
    bgColor: '#d3003d'
  })
];
// organizationTypes


export const ingredientTypes = [
  new SetType({
    name: 'vegan',
    role: 'ingredient',
    priority: 1,
    id: '101',
    color: '#fff',
    bgColor: '#d3003d'
  }),
  new SetType({
    name: 'milk_products',
    role: 'ingredient',
    priority: 2,
    id: '102',
    color: '#fff',
    bgColor: '#d3003d'
  }),
  new SetType({
    name: 'cereals',
    role: 'ingredient',
    priority: 3,
    id: '103',
    color: '#fff',
    bgColor: '#d3003d'
  }),
  new SetType({
    name: 'fruits',
    role: 'ingredient',
    priority: 4,
    id: '104',
    color: '#fff',
    bgColor: '#d3003d'
  }),
  new SetType({
    name: 'berries',
    role: 'ingredient',
    priority: 5,
    id: '105',
    color: '#fff',
    bgColor: '#d3003d'
  }),
  new SetType({
    name: 'meat',
    role: 'ingredient',
    priority: 6,
    id: '106',
    color: '#fff',
    bgColor: '#d3003d'
  }),
  new SetType({
    name: 'eggs',
    role: 'ingredient',
    priority: 7,
    id: '107',
    color: '#fff',
    bgColor: '#d3003d'
  }),
  new SetType({
    name: 'beans',
    role: 'ingredient',
    priority: 8,
    id: '108',
    color: '#fff',
    bgColor: '#d3003d'
  }),
  new SetType({
    name: 'bread',
    role: 'ingredient',
    priority: 9,
    id: '109',
    color: '#fff',
    bgColor: '#d3003d'
  }),
  new SetType({
    name: 'vegetables',
    role: 'ingredient',
    priority: 10,
    id: '110',
    color: '#fff',
    bgColor: '#d3003d'
  })
];
// ingredientTypes


export const dishTypes = [
  new SetType({
    role: 'dish',
    priority: 1,
    id: '201',
    name: 'vegan',
    color: '#fff',
    bgColor: '#d3003d'
  }),
  new SetType({
    role: 'dish',
    priority: 2,
    id: '202',
    name: 'hot_dishes',
    color: '#fff',
    bgColor: '#d3003d'
  }),
  new SetType({
    role: 'dish',
    priority: 3,
    id: '203',
    name: 'desserts',
    color: '#fff',
    bgColor: '#d3003d'
  }),
  new SetType({
    role: 'dish',
    priority: 4,
    id: '204',
    name: 'meat_dishes',
    color: '#fff',
    bgColor: '#d3003d'
  }),
  new SetType({
    role: 'dish',
    priority: 5,
    id: '205',
    name: 'soups',
    color: '#fff',
    bgColor: '#d3003d'
  }),
  new SetType({
    role: 'dish',
    priority: 6,
    id: '206',
    name: 'salads',
    color: '#fff',
    bgColor: '#d3003d'
  })
];
// dishTypes
