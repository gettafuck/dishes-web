import {
	HttpRequest, 
	HttpEvent,
	HttpHandler,
	HttpInterceptor,
	HttpResponse
} from '@angular/common/http';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs';

import { BackendResponse } from '../app/core/models/backend-response/BackendResponse';


@Injectable()
export class LastHttpInterceptorInChain implements HttpInterceptor {

	constructor() { }

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		// console.log(`\n${req.method} ${req.url} was intercepted by mock backend (LastHttpInterceptorInChain)`);

		const response: BackendResponse<any> = new BackendResponse({
			data: null,
			statusText: 'Not Found for ' + req.url,
		});

		// return `Not Found` error
		// if non of previous interceptors in the chain returned a response
		return of( new HttpResponse({body: response}) );

	}
}
