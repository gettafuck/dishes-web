import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageComponent } from './home-page/home-page.component';
import { HomeScreenModule } from '../screens/home-screen/home-screen.module';
import { SearchScreenModule } from '../screens/search-screen/search-screen.module';
import { HeaderModule } from '../ui-elements/header/header.module';

@NgModule({
  imports: [
    CommonModule,
    HeaderModule,
    HomeScreenModule,
    SearchScreenModule
  ],
  declarations: [HomePageComponent]
})
export class HomePageModule { }
