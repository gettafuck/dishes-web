import { EntitySet } from '../entity-set/EntitySet';
/**
 * @info: member of an entity types. For example Organization entity has property 'types';
 * to describe each 'types' member we use ISetType interface.
 * In user interface(for ingredient case at least) it will be something like tag or material design chip,
 * but colored and without a border.
 * @reference: chip example [png]
 * https://material.io/design/assets/1EpGNomAHfyoywofHEqzfoSvp3vZs6zOz/outlined-input-chips-states.png
 * @refarence: Material Design Chips
 * https://material.io/design/components/chips.html
 */
export interface ISetType {
  name: string; /**
   * @info: e.g. vagetables, milk_products sets with role `ingredient`
   *  hot_dishes, desserts sets with role `dish`
   *  restaurant, hotel sets with role `organization`
   */
  role: EntitySet; /**
   * @info: to select only `ingredient` types (sets)
   *  or only `dish` types (sets)
   *  or only `organization` types (sets)
   * 
   *  use 'role' property.
   *  roles: ingredient, dish, organization
   * 
   *  name - set name
   *  role - what are members of the set
   *  e.g. `name`=`vegetables`
   *       `role`=`ingredient`
   *  that means every member of `vegetables` set is ingredient
   * 
   *  or `name`=`restaurant`
   *     `role`=`organization`
   *  that means `restaurant` is a set of organizations
   */
  priority: number; // rises from infinity to 1; 1 - the highest
  id: string;
  color: string; // text color
  bgColor: string; // background color
}
