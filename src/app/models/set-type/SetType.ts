import { ISetType } from './ISetType';
import { EntitySet } from '../entity-set/EntitySet';

export class SetType implements ISetType {
  role: EntitySet;
  priority: number;
  id: string;
  name: string;
  color: string;
  bgColor: string;

  constructor(o: ISetType = <ISetType>{}) {
    this.role = o.role || '';
    this.priority = o.priority || 1;
    this.id = o.id || '';
    this.name = o.name || '';
    this.color = o.color || '';
    this.bgColor = o.bgColor || '';
  }

  /**
   * @info: just for output
   */
  get splitName() {
    return this.name.replace('_', ' ');
  }
}
