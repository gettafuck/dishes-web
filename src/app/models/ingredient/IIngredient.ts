import { ISetType } from '../set-type/ISetType';
import { IImage } from '../image/IImage';
import { IUser } from '../user/IUser';

export interface IIngredient {
  types: ISetType[]; /**
   * @info: type names: vegan, milk_products, cereals, 
   *                    fruits, berries, etc...
   */
  images: IImage[]; /**
   * @info: you can find an ingredient linear icon in image with role 'logo',
   * cover image - in image with role 'cover'
   */
  creatorId: string;
  creator: IUser; // will be put in backend by creatorId
  approverId: string;
  approver: IUser; /**
   * @info: will be put in backend by approverId;
   * any authorized user can propose an ingredient,
   * a request to add a new ingredient will be seen in admin dashboard,
   * so creation of an ingredient must be approved by admin or moderator.
   * If moderator or admin creates an ingredient in admin dashboard, it mustn't be approved
   * and a creator automatically becomes an approver.
   */
  id: string;
  name: string;
  weight: number; /** gramms
   * @info: weight of an ingredient in particular dish
   */
}
