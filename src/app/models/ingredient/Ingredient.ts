import { IIngredient } from './IIngredient';
import { ISetType } from '../set-type/ISetType';
import { IImage } from '../image/IImage';
import { IUser } from '../user/IUser';
import { Image } from '../image/Image';

import * as _ from 'lodash';

export class Ingredient implements IIngredient {
  types: ISetType[];
  images: IImage[];
  creatorId: string;
  creator: IUser;
  approverId: string;
  approver: IUser;
  id: string;
  name: string;
  weight: number;

  constructor(o: IIngredient = <IIngredient>{}) {
    this.types = o.types || [];
    this.images = o.images || [];
    this.creatorId = o.creatorId || '';
    this.creator = o.creator || <IUser>{};
    this.approverId = o.approverId || '';
    this.approver = o.approver || <IUser>{};
    this.id = o.id || '';
    this.name = o.name || '';
    this.weight = o.weight || 0;
  }

  setWeight(w: number) { this.weight = w; }

	public getLogo(): Image {
		return this.getImage('logo');
	}

	public getCover(): Image {
		return this.getImage('cover');
	}

	private getImage(role: string): Image {
		return _.find(this.images, (image: Image) => image.role === role);
	}
}
