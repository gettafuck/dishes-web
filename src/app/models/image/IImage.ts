import { IUser } from '../user/IUser';
import { EntitySet } from '../entity-set/EntitySet';

export interface IImage {
  id: string;
  role: string; /**
   * @info: image roles
   * cover - cover image of an organization page, cover image of an user page
   * logo - logo image of an organization, avatar of an user
   */
  set: EntitySet; /**
   * @info: to select only `ingredient` images 
   *  or only `dish` images 
   *  or only `organization` images 
   *  or only `user` images
   * use 'set' property.
   * sets: ingredient, dish, organization, user
   */
  creatorId: string;
  creator: IUser; // will be put in backend by creatorId
  url: string;
  description: string;
}
