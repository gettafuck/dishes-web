import { IImage } from '../image/IImage';
import { IUser } from '../user/IUser';
import { EntitySet } from '../entity-set/EntitySet';

export class Image implements IImage {
  id: string;
  role: string;
  set: EntitySet;
  creatorId: string;
  creator: IUser;
  url: string;
  description: string;

  constructor(o: IImage = <IImage>{}) {
    this.id = o.id || '';
    this.role = o.role || '';
    this.set = o.set || '';
    this.creatorId = o.creatorId || '';
    this.creator = o.creator || <IUser>{};
    this.url = o.url || '';
    this.description = o.description || '';
  }
}
