import { ISearchComponentConfig } from './ISearchComponentConfig';
import { SearchComponentMode } from './SearchComponentMode';

export class SearchComponentConfig implements ISearchComponentConfig {
	url: string;
	parameterName: string;
	placeholder: string;
	mode: SearchComponentMode;
	autocompleteUrl?: string;

	constructor(o: ISearchComponentConfig = <ISearchComponentConfig>{}) {
		this.url = o.url || '';
		this.parameterName = o.parameterName || '';
		this.placeholder = o.placeholder || '';
		this.mode = o.mode || 'simple';
		this.autocompleteUrl = o.autocompleteUrl || '';
	}
}
