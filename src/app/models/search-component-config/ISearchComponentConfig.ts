import { SearchComponentMode } from './SearchComponentMode';

export interface ISearchComponentConfig {
	url: string; /**
	 * @info: url to search, e.g. `http:domain.name/dishes`
	 */

	parameterName: string; /** e.g. parameterName = 'ingredients'
	 * parameter name is used in url for search, e.g. `http:domain.name/dishes?{parameterName}=value1,value2`
	 */

	placeholder: string; /**
	 * @info: search input placeholder;
	 */

	mode: SearchComponentMode;

	autocompleteUrl?: string; /**
	 * @info: url to load autocomplete items, e.g. `http:domain.name/ingredients`
	 * 
	 *  Autocomplete item is a value of {parameterName}. A user can choose one or more autocomplete items.
	 * 
	 * 	In `groupedAutocomlete` mode SearchComponent automatically adds search parameters `group` and `page` 
	 * 	to make a pagination inside each group of autocomplete items.
	 * 
	 * 	In `autocomlete` mode SearchComponent automatically adds search parameter `page` 
	 * 	to make a pagination inside a list of autocomplete items.
	 * 
	 * 	`name` search parameter is used while searching inside a list of autocomplete items.
	 * 
	 * @needed: autocomplete, groupedAutocomplete modes
	 */
}
