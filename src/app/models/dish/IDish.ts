import { IIngredient } from '../ingredient/IIngredient';
import { ISetType } from '../set-type/ISetType';
import { IOrganization } from '../organization/IOrganization';
import { IImage } from '../image/IImage';
import { IUser } from '../user/IUser';

export interface IDish {
  types: ISetType[]; /**
   * @info: type names: vegan, hot_dishes, desserts, 
   *                    meat_dishes, soups, salads, etc...
   */
  images: IImage[]; /**
   * @info: you can find an logo image in image with role 'logo',
   * cover image - in image with role 'cover'
   */
  organizationId: string;
  organization: IOrganization; // will be put in backend by organizationId
  creatorId: string;
  creator: IUser; // will be put in backend by creatorId
  id: string;
  name: string;
  ingredients: IIngredient[];
}
