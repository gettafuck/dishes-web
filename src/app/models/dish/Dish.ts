import { IDish } from './IDish';
import { ISetType } from '../set-type/ISetType';
import { IImage } from '../image/IImage';
import { IOrganization } from '../organization/IOrganization';
import { IUser } from '../user/IUser';
import { IIngredient } from '../ingredient/IIngredient';

/**
 * @info: see all property descriptions in the implemented interface.
 */

export class Dish implements IDish {
  types: ISetType[];
  images: IImage[];
  organizationId: string;
  organization: IOrganization;
  creatorId: string;
  creator: IUser;
  id: string;
  name: string;
  ingredients: IIngredient[];

  constructor(o: IDish = <IDish>{}) {
    this.types = o.types || [];
    this.images = o.images || [];
    this.organizationId = o.organizationId || '';
    this.organization = o.organization || <IOrganization>{};
    this.creatorId = o.creatorId || '';
    this.creator = o.creator || <IUser>{};
    this.id = o.id || '';
    this.name = o.name || '';
    this.ingredients = o.ingredients || [];
  }
}
