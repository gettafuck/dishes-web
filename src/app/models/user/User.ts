import { IUser } from './IUser';
import { Image } from '../image/Image';

export class User implements IUser {
  membership: string;
  roles: string[];
  images: Image[];
  id: string;
  name: string;
  lastName: string;

  constructor(o: IUser = <IUser>{}) {
    this.membership = o.membership || '';
    this.roles = o.roles || [];
    this.images = o.images || [];
    this.id = o.id || '';
    this.name = o.name || '';
    this.lastName = o.lastName || '';
  }
}
