import { IImage } from '../image/IImage';

export interface IUser {
  membership: string; /**
   * @note: at first months after the application published, we DON'T have any membership feature
   * but then, after we will get a lot of users and organizations,
   * we will activate a membership feature.
   * 
   * @info: to access some features, an authorized user needs to have an according membership plan.
   * membersip types:
   * basic - when an user creates an account he has a basic membership
   * middle
   * pro
   * gold
   */
  roles: string[]; /**
   * @info: roles
   * empty array means that an user is unauthorized
   * user - authorized user with basic CRUD rights; cannot create
   * moderator - like admin but doesn't have some important rights.
   *             Just generates a content, cannot approve reqests to add an ingredient,
   *             cannot change membership of any user.
   * admin - has all rights but cannot delete/update/create other admins and superadmin
   * superadmin - has all rights; can delete/update/create any admin.
   */
  images: IImage[]; /**
   * @info: you can find an avatar image in image with role 'logo',
   * cover image - in image with role 'cover'
   */
  id: string;
  name: string;
  lastName: string;
}
