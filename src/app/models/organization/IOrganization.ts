import { ISetType } from '../set-type/ISetType';
import { IDish } from '../dish/IDish';
import { IImage } from '../image/IImage';
import { IUser } from '../user/IUser';

export interface IOrganization {
  types: ISetType[]; /**
   * @info: type names: restaurant, hotel, night_club, 
   *                    coworking, cafe, bakery,
   *                    coffee_shop, tea_shop, wine_pub, 
   *                    beer_pub
   */
  images: IImage[]; /**
   * @info: you can find a logo image in image with role 'logo',
   * cover image - in image with role 'cover'
   */
  creatorId: string;
  creator: IUser; // will be put in backend by creatorId
  ownerId: string;
  owner: IUser; /** will be put in backend by ownerId
   * @info: an organization can be created by admin
   * but then an ownership can be passed to an user with some membership.
   */
  id: string;
  name: string;
  description: string;
  dishes: IDish[];
}
