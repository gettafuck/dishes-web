import { IOrganization } from "./IOrganization";
import { ISetType } from "../set-type/ISetType";
import { IImage } from "../image/IImage";
import { IUser } from "../user/IUser";
import { IDish } from "../dish/IDish";

export class Organization implements IOrganization {
  types: ISetType[];
  images: IImage[];
  creatorId: string;
  creator: IUser;
  ownerId: string;
  owner: IUser;
  id: string;
  name: string;
  description: string;
  dishes: IDish[];

  constructor(o: IOrganization = <IOrganization>{}) {
    this.types = o.types || [];
    this.images = o.images || [];
    this.creatorId = o.creatorId || '';
    this.creator = o.creator || <IUser>{};
    this.ownerId = o.ownerId || '';
    this.owner = o.owner || <IUser>{};
    this.id = o.id || '';
    this.name = o.name || '';
    this.description = o.name || '';
    this.dishes = o.dishes || [];
  }
}
