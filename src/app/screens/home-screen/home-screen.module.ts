import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeScreenComponent } from './home-screen/home-screen.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [HomeScreenComponent],
  exports: [HomeScreenComponent]
})
export class HomeScreenModule { }
