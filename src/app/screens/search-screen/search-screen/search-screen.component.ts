import { Component, OnInit } from '@angular/core';

import { PaginationItem } from '../../../core/models/pagination-item/PaginationItem';
import { Dish } from '../../../models/dish/Dish';

import { PaginatedGroup } from '../../../core/models/paginated-group/PaginatedGroup';
import { Ingredient } from '../../../models/ingredient/Ingredient';

import { SearchComponentConfig } from '../../../models/search-component-config/SearchComponentConfig';


@Component({
  selector: 'app-search-screen',
  templateUrl: './search-screen.component.html',
  styleUrls: ['./search-screen.component.scss']
})
export class SearchScreenComponent {

  searchPage: number = 1;

  searchComponentConfig: SearchComponentConfig = new SearchComponentConfig({
  	url: 'dishes',
  	parameterName: 'ingredients',
  	placeholder: 'Ingredients...',
    mode: 'groupedAutocomplete',
  	autocompleteUrl: 'ingredients'
  });

  pageWithDishes: PaginationItem<Dish>;

  ingredients: PaginatedGroup <Ingredient> [];

  constructor() { }

  handleOnData(data: PaginationItem<any>) {
    console.log('\n SearchScreenComponent: got searched data : \n', data);

    this.pageWithDishes = data;
    this.searchPage = data.number;
  }

  onPageChanged(newPage: number) {
    console.log('\n SearchScreenComponent: got new page number from pagination-component : ', newPage);
    this.searchPage = newPage;
  }

}
