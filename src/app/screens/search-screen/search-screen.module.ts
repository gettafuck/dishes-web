import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchScreenComponent } from './search-screen/search-screen.component';

import { SearchModule } from '../../ui-elements/search/search.module';

import { PaginationModule } from '../../ui-elements/pagination/pagination.module';

@NgModule({
  imports: [
    CommonModule,

    SearchModule,

    PaginationModule
  ],
  declarations: [SearchScreenComponent],
  exports: [SearchScreenComponent]
})
export class SearchScreenModule { }
