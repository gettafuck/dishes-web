import { Injectable } from '@angular/core';
import { Url } from '../../models/url/Url';

import * as _ from 'lodash';
import { KeyValues } from '../../models/key-values/KeyValues';

@Injectable({
  providedIn: 'root'
})
export class StringService {

  constructor() { }

  parseUrl(url: string): Url {

    const url_: Url = new Url({
      origin: url,
      queryParams: []
    });

    const search: string = url.split('?')[1];
    if (!search) {
      return url_;
    }

    const parts: string[] = search.split('&');

    _.forEach(parts, (part: string) => {
      url_.queryParams.push( new KeyValues({
        key: part.split('=')[0],
        values: part.split('=')[1] ? part.split('=')[1].split(',') : []
      }) );
    });

    return url_;
  }
}
