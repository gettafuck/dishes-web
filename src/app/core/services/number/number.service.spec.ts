import { TestBed, inject } from '@angular/core/testing';

import { NumberService } from './number.service';
import { NumberChangeParams } from '../../models/number-change-params/NumberChangeParams';

describe('NumberService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NumberService]
    });
  });

  it('should be created', inject([NumberService], (service: NumberService) => {
    expect(service).toBeTruthy();
  }));

  
  it('watchRangeCircular() : `current` should be lower or equal `max` after change', inject([NumberService], (service: NumberService) => {
    const params: NumberChangeParams = new NumberChangeParams({
      current: 5,
      min: 1,
      max: 7
    });
    expect(service.watchRange(params) <= params.max).toBe(true);
  }));
  
  it('watchRangeCircular() : `current` should be upper or equal `min` after change', inject([NumberService], (service: NumberService) => {
    const params: NumberChangeParams = new NumberChangeParams({
      current: 5,
      min: 1,
      max: 7,
      decrease: true
    });
    expect(service.watchRange(params) >= params.min).toBe(true);
  }));


  it('watchRangeCircular() : `current` should be equal `min` after change', inject([NumberService], (service: NumberService) => {
    const params: NumberChangeParams = new NumberChangeParams({
      current: 7,
      min: 1,
      max: 7
    });
    expect(service.watchRange(params) == params.min).toBe(true);
  }));

  it('watchRangeCircular() : `current` should be equal `max` after change', inject([NumberService], (service: NumberService) => {
    const params: NumberChangeParams = new NumberChangeParams({
      current: 1,
      min: 1,
      max: 7,
      decrease: true
    });
    expect(service.watchRange(params) == params.min).toBe(true);
  }));

});
