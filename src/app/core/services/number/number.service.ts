import { Injectable } from '@angular/core';
import { NumberChangeParams } from '../../models/number-change-params/NumberChangeParams';

@Injectable({
  providedIn: 'root'
})
export class NumberService {

  constructor() { }

  /**
   * @info: The method takes a current number, increases or decreases it
   *  and cares about this number don't move out from a range.
   *  It resets to max if we are trying to decrease a current number and it becomes lower a range.
   *  It resets to min if we are trying to increase a current number and it becomes upper a range
   *  By default the method increases a current number.
   *  Set `true` to `decrease` parameter to decrease a current number.
   * 
   * @use: This method can be used in pagination.
   */
  watchRangeCircular(p: NumberChangeParams): number {

    let current: number = p.current; /**
     * @info: original `current` should not be changed inside of the method.
     */

    if (p.decrease && current - p.interval >= p.min) {
      current -= p.interval;
    } else if (p.decrease && current - p.interval < p.min) {
      current = p.max; /**
       * @info: reset to max if we are trying to decrease a current number and it becomes lower a range
       */
    } else if (!p.decrease && current + p.interval <= p.max) {
      current += p.interval;
    } else if (!p.decrease && current + p.interval > p.max) {
      current = p.min; /**
       * @info: reset to min if we are trying to increase a current number and it becomes upper a range
       */
    }

    return current;
  }

  /**
   * @info: 
   * The method takes a current number, increases or decreases it and cares to this number don't move out from a range.
   * By default the method increases a current number.
   * Set `true` to `decrease` parameter to decrease a current number.
   * This method is used when we need to change a current number and do not move out from a range
   */
  watchRange(p: NumberChangeParams): number {

    let current: number = p.current; /**
     * @info: original `current` should not be changed inside of the method.
     */

    if (p.decrease && current - p.interval >= p.min ) {
      current -= p.interval;
    }
    if (!p.decrease && current + p.interval <= p.max ) {
      current += p.interval;
    }

    return current;
  }

}
