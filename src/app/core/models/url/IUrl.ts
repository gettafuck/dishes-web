import { KeyValues } from '../key-values/KeyValues';

export interface IUrl {
	origin: string;
	queryParams: KeyValues[];
}
