import { IUrl } from './IUrl';
import { KeyValues } from '../key-values/KeyValues';

import * as _ from 'lodash';

export class Url implements IUrl {
	origin: string;
	queryParams: KeyValues[];

	constructor(o: IUrl = <IUrl>{}) {
		this.origin = o.origin || '';
		this.queryParams = o.queryParams || [];
	}

	get(key: string): KeyValues {
		return _.find(this.queryParams, (qp: KeyValues) => qp.key == key) || new KeyValues();
	}
}
