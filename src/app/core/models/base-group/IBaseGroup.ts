export interface IBaseGroup<ElementType> {
	name: string;
	elements: ElementType[];
}
