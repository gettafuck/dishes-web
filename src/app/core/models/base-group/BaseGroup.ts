import { IBaseGroup } from './IBaseGroup';

export class BaseGroup<ElementType> implements IBaseGroup<ElementType> {
	name: string;
	elements: ElementType[];

	constructor(o: IBaseGroup<ElementType> = <IBaseGroup<ElementType>>{}) {
		this.name = o.name || '';
		this.elements = o.elements || [];
	}
}
