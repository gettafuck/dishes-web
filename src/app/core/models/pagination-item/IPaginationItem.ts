/**
 * @info: PaginationItem represents a page from list of pages
 */

export interface IPaginationItem<ElementType> {

	elements: ElementType[]; /**
	 * @info: elements of a page
	 */

	number: number; /**
	 * @info: number of current page,
	 * starts from 1
	 */

	allNumber: number; /**
	 * @info: amount of all pages
	 */
}
