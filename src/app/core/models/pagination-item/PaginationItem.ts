import { IPaginationItem } from './IPaginationItem';

export class PaginationItem<ElementType> implements IPaginationItem<ElementType> {
	elements: ElementType[];
	number: number;
	allNumber: number;

	constructor(o: IPaginationItem<ElementType> = <IPaginationItem<ElementType>>{}) {
		this.elements = o.elements || [];
		this.number = o.number || 0;
		this.allNumber = o.allNumber || 0;
	}
}
