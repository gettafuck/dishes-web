export interface IKeyValues {
	key: string;
	values: string[];
}
