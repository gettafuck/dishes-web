import { IKeyValues } from './IKeyValues';

export class KeyValues implements IKeyValues {
	key: string;
	values: string[];

	constructor(o: IKeyValues = <IKeyValues>{}) {
		this.key = o.key || '';
		this.values = o.values || [];
	}
}
