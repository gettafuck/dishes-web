import { PaginationItem } from '../pagination-item/PaginationItem';
import { SetType } from '../../../models/set-type/SetType';

export interface IPaginatedGroup<PageElementType> {
	group: SetType; /**
	 * @info: group with name and other properties, such as bgColor, color, etc.
	 */
	page: PaginationItem<PageElementType>; /**
	 * @info: page with elements that belong to the group
	 */
}
