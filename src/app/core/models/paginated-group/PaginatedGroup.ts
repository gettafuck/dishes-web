import { PaginationItem } from '../pagination-item/PaginationItem';
import { IPaginatedGroup } from './IPaginatedGroup';
import { SetType } from '../../../models/set-type/SetType';

export class PaginatedGroup<PageElementType> implements IPaginatedGroup<PageElementType> {
	group: SetType;
	page: PaginationItem<PageElementType>;

	constructor(o: IPaginatedGroup<PageElementType> = < IPaginatedGroup<PageElementType> >{}) {
		this.group = o.group || new SetType();
		this.page = o.page || new PaginationItem();		
	}
}
