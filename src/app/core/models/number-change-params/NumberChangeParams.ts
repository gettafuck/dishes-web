import { INumberChangeParams } from './INumberChangeParams';

export class NumberChangeParams implements INumberChangeParams {
	current: number;
	min: number;
	max: number;
	decrease?: boolean;
	interval?: number;

	constructor(o: INumberChangeParams = <INumberChangeParams>{}) {
		this.decrease = o.decrease || false;
		this.interval = o.interval || 1;
		this.current = o.current || 0;
		this.min = o.min || 0;
		this.max = o.max || 0;
	}
}
