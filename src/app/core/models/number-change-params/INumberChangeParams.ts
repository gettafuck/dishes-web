export interface INumberChangeParams {
	current: number; /**
	 * @info: a current number that we need to increase or decrease
	 */
	min: number; /**
	 * @info: a lower bound of a range
	 */
	max: number; /**
	 * @info: an upper bound of a range
	 */
	decrease?: boolean; /**
	 * @info: set `true` to decrease a current number. By default a current number is increased.
	 */
	interval?: number; /**
	 * @info: interval of increasing or decreasing. By default it equals 1.
	 */
}
