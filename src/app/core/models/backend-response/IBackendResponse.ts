export interface IBackendResponse<DataType> {
	data: DataType;
	statusText: string;
}
