import { IBackendResponse } from './IBackendResponse';

export class BackendResponse<DataType> implements IBackendResponse<DataType> {
	data: DataType;
	statusText: string;

	constructor(o: IBackendResponse<DataType> = <IBackendResponse<DataType>>{}) {
		this.data = o.data || <DataType>{};
		this.statusText = o.statusText || '';
	}
}
