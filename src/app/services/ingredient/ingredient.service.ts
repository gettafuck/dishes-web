import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Ingredient } from '../../models/ingredient/Ingredient';
import { PaginatedGroup } from '../../core/models/paginated-group/PaginatedGroup';
import { HttpClient } from '@angular/common/http';
import { BackendResponse } from '../../core/models/backend-response/BackendResponse';

@Injectable({
  providedIn: 'root'
})
export class IngredientService {

  constructor(
    private http: HttpClient
  ) { }

  getIngredients(): Observable< PaginatedGroup<Ingredient> [] > {
    return this.http.get('/ingredients')
      .map((response: BackendResponse< PaginatedGroup<Ingredient> [] >) => {
        /**
         * @test
         */
        console.log('IngredientService.getIngredients : response \n', response);

        return response.data;
      });
  }
}
