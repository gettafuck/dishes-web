import { TestBed, inject } from '@angular/core/testing';

import { DishService } from './dish.service';
import { Dish } from '../../models/dish/Dish';

import { HttpClient, HttpHandler } from '@angular/common/http';

describe('DishService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        HttpClient,
        DishService
      ]
    });
  });

  it('should be created', inject([DishService], (service: DishService) => {
    expect(service).toBeTruthy();
  }));

  it('getDishes() returns dishes from Observable', inject([DishService], (service: DishService) => {
    service.getDishes()
      .subscribe((dishes: Dish[]) => {
        console.log('DishService.getDishes() : dishes \n', dishes);
      });
    expect(service.getDishes()).toBeTruthy();
  }));
});
