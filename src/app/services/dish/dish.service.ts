import { Injectable } from '@angular/core';

import { HttpClient, HttpResponseBase, HttpResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';

import { Dish } from '../../models/dish/Dish';
import { BackendResponse } from '../../core/models/backend-response/BackendResponse';
import { PaginationItem } from '../../core/models/pagination-item/PaginationItem';

@Injectable({
  providedIn: 'root'
})
export class DishService {

  constructor(
    private http: HttpClient
  ) { }

  getDishes(): Observable< PaginationItem<Dish> > {
    return this.http.get('/dishes')
      .map((response: BackendResponse< PaginationItem<Dish> >) => {
        /**
         * @test
         */
        console.log('DishService.getDishes : response ', response);

        return response.data;
      });
  }

}
