import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { BackendResponse } from '../../core/models/backend-response/BackendResponse';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(
    private http: HttpClient
  ) { }

  search(url: string): Observable<BackendResponse<any>> {
    return this.http.get(url)
      .map((response: BackendResponse<any>) => {
        /**
         * @test
         */
        console.log(`\n SearchService search - ${ url } \n`, response);
        return response;
      });
  }
}
