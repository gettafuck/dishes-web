import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';

import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { APP_BASE_HREF } from '@angular/common';

import { DishesHttpInterceptor } from '../fake-backend/dishes/DishesHttpInterceptor';

describe('AppComponent', () => {
	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [
				AppComponent
			],

			providers: [,
				{ provide: HTTP_INTERCEPTORS, useClass: DishesHttpInterceptor, multi: true },
				{ provide: APP_BASE_HREF, useValue: '/' }
			]

		}).compileComponents();
	}));

	it('should create the app', async(() => {
		const fixture = TestBed.createComponent(AppComponent);
		const app = fixture.debugElement.componentInstance;
		expect(app).toBeTruthy();
	}));

	/**
	 * @examples
	 */
		/*
			it(`should have as title 'app'`, async(() => {
				const fixture = TestBed.createComponent(AppComponent);
				const app = fixture.debugElement.componentInstance;
				expect(app.title).toEqual('app');
			}));

			it('should render title in a h1 tag', async(() => {
				const fixture = TestBed.createComponent(AppComponent);
				fixture.detectChanges();
				const compiled = fixture.debugElement.nativeElement;
				expect(compiled.querySelector('h1').textContent).toContain('Welcome to app!');
			}));
		*/
});
