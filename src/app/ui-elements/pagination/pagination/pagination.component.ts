import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { NumberService } from '../../../core/services/number/number.service';
import { NumberChangeParams } from '../../../core/models/number-change-params/NumberChangeParams';

import * as _ from 'lodash';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  @Input('number') _number: number; /**
   * @info: number of the current page 
   */
  @Input('allNumber') _allNumber: number; /**
   * @info: number of all
   */
  @Output('numberChanged') _numberChanged: EventEmitter<number> = new EventEmitter(); /**
   * @info: event that fires if number of the current page was changed
   */

  constructor(
    private numberService: NumberService
  ) { }

  ngOnInit() {
  }

  next() {
    const params: NumberChangeParams = new NumberChangeParams({
      current: this._number,
      min: 1,
      max: this._allNumber
    });

    this._number = this.numberService.watchRangeCircular(params);

    this._numberChanged.emit( this._number );
  }

  prev() {
    const params: NumberChangeParams = new NumberChangeParams({
      current: this._number,
      min: 1,
      max: this._allNumber,
      decrease: true
    });

    this._number = this.numberService.watchRangeCircular(params);

    this._numberChanged.emit( this._number );
  }

  setPage(n: number) {
    this._number = n;
    this._numberChanged.emit( this._number );
  }

  getPagesRange() {
    return _.range(1, this._allNumber + 1);
  }
}
