/**
 * @info: 
 *  takes query params, url to back-end,
 *  and returns paginated search results.
 */

/**
 * @modes: Modes of the ui-element
 *  simple
 * 
 *  autocomplete
 * 
 *  groupedAutocomplete
 * 
 *    @scenario: pagination over `initial` list of autocomplete items #way1
 *      #note filtering, grouping, pagination must be done on back-end
 *      1) load all groups of autocomplete items with the first page in each group
 *      2) request a particular page of a group (e.g. page 2)
 *      3) select autocomplete item 
 *      4) make search with selected autocomplete item(s)
 * 
 *    @scenario: pagination over `filtered` list of autocomplete items #way2
 *      #note filtering, grouping, pagination must be done on back-end
 *      1) load all groups of autocomplete items with the first page in each group
 *      2) type a letter in search input (e.g. letter `a`)
 *      3) get groups with `filtered` autocomplete items with the first page in each group.
 *         Groups that don't contain any item are filtered in back-end
 *      4) request a particular page of a group with `filtered` autocomplete items (e.g. page 2)
 *      5) select autocomplete item 
 *      6) make search with selected autocomplete item(s)
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchComponent } from './search/search.component';

import { ReactiveFormsModule } from '@angular/forms';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

import { PaginationModule } from '../pagination/pagination.module';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

@NgModule({
  imports: [
    CommonModule,

    ReactiveFormsModule,
    
    MatChipsModule,
    MatIconModule,
    MatInputModule,
    MatAutocompleteModule,

    PaginationModule,

    // perfect scrollbar 
    PerfectScrollbarModule
  ],
  declarations: [SearchComponent],
  exports: [SearchComponent]
})
export class SearchModule { }
