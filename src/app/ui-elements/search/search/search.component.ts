import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';

import { ElementRef, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';

import { Image } from '../../../models/image/Image';

import { PaginationItem } from '../../../core/models/pagination-item/PaginationItem';

import { BackendResponse } from '../../../core/models/backend-response/BackendResponse';

import { SearchService } from '../../../services/search/search.service';

import { SearchComponentConfig } from '../../../models/search-component-config/SearchComponentConfig';
import { PaginatedGroup } from '../../../core/models/paginated-group/PaginatedGroup';

import * as _ from 'lodash';


interface IBaseAutocompleteItem {
  name: string;
  
  getLogo(): Image;
  getCover(): Image;
}

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnChanges {

  @Input('config') _config: SearchComponentConfig;
  @Input('page') _page: number; /**
   * @info: SearchComponent automatically reacts on `page` parameter changes, 
   *  make search with `page` value and emits `onData` event
   */

  @Output('onData') _onData: EventEmitter< PaginationItem<any> > = new EventEmitter();

  /**
   * @warn: used only in `groupedAutocomplete` mode
   */
  autocompleteGroups: PaginatedGroup<IBaseAutocompleteItem> [];


  autocompleteItemCtrl = new FormControl();

  /**
   * @info: filtered autocomplete items
   */
  searchedAutocompleteItems: IBaseAutocompleteItem[] = [];

  /**
   * @info: selected autocomplete items to search a data
   */
  selectedAutocompleteItems: IBaseAutocompleteItem[] = [];

  /**
   * @info: about the second param
   * https://stackoverflow.com/a/56752507
   */
  @ViewChild('autocompleteItemInput', {static: true}) autocompleteItemInput: ElementRef;

  matChipRemovable: boolean = true;
  matChipSelectable: boolean = true;

  autocompleteHidden: boolean = true;

  constructor(
    private searchService: SearchService
  ) { }

  ngOnChanges(changes: SimpleChanges) {
    console.log('\nSearchComponent: onChanges - data: \n', changes);
    this.getSearchedData(changes._page.currentValue);
  }

  ngOnInit() {

    this.getSearchedData();

    if (this._config.mode == 'groupedAutocomplete') {
      this.getAutocompleteGroups();

      this.autocompleteItemCtrl.valueChanges
        .subscribe(value => {
          console.log('\n\n A new value in the search input - ', value);

          this.searchAutocompleteGroups(value);

          this.autocompleteHidden = false;
        });
    }

    /**
     * @test: interceptor test
     */
      // this.searchService.search('/dishes?ingredients=cheese,rucola,dill&page=2')
      //   .subscribe();

    /**
     * @test: interceptor test
     */
      // this.searchService.search(`/ingredients?group=vegan&page=2`)
      //   .subscribe();
  }
  // ngOnInit - end

  /**
   * @info: searching inside a list of autocomplete items
   */
  searchAutocompleteItems(autocompleteItem: IBaseAutocompleteItem) {
  }

  /**
   * @info: remove autocomplete item from search
   */
  removeAutocompleteItem(index: number) {
    this.selectedAutocompleteItems.splice(index, 1);
    this.getSearchedData();
  }

  selectAutocompleteItem(item: IBaseAutocompleteItem): void {
    // this.autocompleteHidden = true;
    // this.autocompleteItemInput.nativeElement.value = '';
    this.selectedAutocompleteItems.push( item );
    this.getSearchedData();
  }



  getAutocompleteGroups() {
    this.searchService.search(`/${this._config.autocompleteUrl}`)
      .subscribe(response => this.autocompleteGroups = response.data);
  }

  getAutocompleteGroupPage(newPage: number, group: PaginatedGroup<any>, groupIndex: number) {
    this.searchService.search(`/${this._config.autocompleteUrl}?group=${group.group.name}&page=${newPage}`)
      .subscribe(response => this.autocompleteGroups[groupIndex] = response.data);
  }



  searchAutocompleteGroups(autocompleteItemName: string) {
    this.searchService.search(`/${this._config.autocompleteUrl}?name=${autocompleteItemName}`)
      .subscribe(response => this.autocompleteGroups = response.data);
  }

  searchAutocompleteGroupPage(newPageNumber: number, autocompleteItemName: string, group: PaginatedGroup<any>, groupIndex: number) {
    this.searchService.search(`/${this._config.autocompleteUrl}?name=${autocompleteItemName}&group=${group.group.name}&page=${newPageNumber}`)
      .subscribe(response => this.autocompleteGroups[groupIndex] = response.data);
  }


  onPageNumberChangedInGroup(newPageNumber: number, group: PaginatedGroup<any>, groupIndex: number) {
    const searchInputValue: string = this.autocompleteItemInput.nativeElement.value;
    if (!searchInputValue) {
      this.getAutocompleteGroupPage(newPageNumber, group, groupIndex);
    } else {
      this.searchAutocompleteGroupPage(newPageNumber, searchInputValue, group, groupIndex);
    }
  }




  getSearchedData(page: number = 1) {
    const selectedAutocompleteItems: string = this.getSelectedAutocompleteItemsString();

    let url = `/${this._config.url}?page=${page}`;

    if (selectedAutocompleteItems.length > 0) {
      url = `/${this._config.url}?${this._config.parameterName}=${selectedAutocompleteItems}&page=${page}`;
    }

    this.searchService.search(url)
      .subscribe((response: BackendResponse<PaginationItem<any>>) => {
        this._onData.emit( response.data );
        this._page = response.data.number;
      });
  }

  getSelectedAutocompleteItemsString(): string {
    let strings = [];
    _.forEach(this.selectedAutocompleteItems, (el: IBaseAutocompleteItem) => strings.push(el.name) );

    return strings.join(',');
  }
}

