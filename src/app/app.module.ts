import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing/app-routing.module';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

//pages
  import { HomePageModule } from './home-page/home-page.module';

// test backend
  import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

  import { DishesHttpInterceptor } from '../fake-backend/dishes/DishesHttpInterceptor';
  import { IngredientsHttpInterceptor } from '../fake-backend/ingredients/IngredientsHttpInterceptor';
  import { LastHttpInterceptorInChain } from '../fake-backend/LastHttpInterceptorInChain';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,

    HttpClientModule,

    //pages
    HomePageModule,

    // perfect scrollbar 
    PerfectScrollbarModule
  ],
  providers: [
    // interceptors
		{ provide: HTTP_INTERCEPTORS, useClass: IngredientsHttpInterceptor, multi: true },
		{ provide: HTTP_INTERCEPTORS, useClass: DishesHttpInterceptor, multi: true },
		{ provide: HTTP_INTERCEPTORS, useClass: LastHttpInterceptorInChain, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
